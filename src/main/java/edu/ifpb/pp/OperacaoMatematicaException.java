package edu.ifpb.pp;

/**
 *
 * @author Ricardo Job
 */
public class OperacaoMatematicaException1 extends RuntimeException {

    public OperacaoMatematicaException() {
    }

    public OperacaoMatematicaException(String message) {
        super(message);
    }

}
